console.log(mdc);

const MDCTextField = mdc.textField.MDCTextField;
const textFields = [].map.call(
  document.querySelectorAll(".mdc-text-field"),
  function (el) {
    return new MDCTextField(el);
  }
); 

const MDCCheckbox = mdc.checkbox.MDCCheckbox;
const checkboxes = [].map.call(
  document.querySelectorAll(".mdc-checkbox"),
  function (el) {
    return new MDCCheckbox(el);
  }
);


const nameInput = document.getElementById('full-name');
const usernameInput = document.getElementById('user-name');
const passwordInput = document.getElementById('enter-password');
const confirmPasswordInput = document.getElementById('confirm-password');
const ageInput = document.getElementById('user-age');
const birthDateInput = document.getElementById('birth-date');

let legalCheckbox = document.getElementById('legal-checkbox');
let termsCheckbox = document.getElementById('terms-checkbox');

const eligibilityForm = document.getElementById('eligibility-form');
const inputs = eligibilityForm.querySelectorAll('input');
let isValid = true;

document.getElementById('eligibility-form').addEventListener('submit', function (event) {
  event.preventDefault();

  console.log(`Full Name: ${nameInput.value}`);
  console.log(`Username: ${usernameInput.value}`);
  console.log(`Enter Password: ${passwordInput.value}`);
  console.log(`Confirm Password: ${confirmPasswordInput.value}`);
  console.log(`Age: ${ageInput.value}`);
  console.log(`Birth Date: ${birthDateInput.value}`);

  if (legalCheckbox.checked) {
    console.log('The user has checked the legal checkbox');
    legalCheckbox = true;
    console.log(legalCheckbox);
  } else {
    console.log('The user has not checked the legal checkbox');
    legalCheckbox = false;
  }

  if (termsCheckbox.checked) {
    console.log('The user has checked the terms checkbox');
    termsCheckbox = true;
    console.log(termsCheckbox);
  } else {
    console.log('The user has not checked the terms checkbox');
    termsCheckbox = false;
  }


  for (let input of inputs) {
    if (input.type !== 'checkbox' && input.value.trim() === '') {
      isValid = false;
      console.log(input.id + ' is blank');
    }
  }

  if (passwordInput.value !== confirmPasswordInput.value || ageInput.value < 13 || !legalCheckbox || !termsCheckbox) {
    isValid = false;
  }

  if (isValid) {
    console.log('The user is eligible');
  } else {
    console.log('The user is ineligible');
  }
});