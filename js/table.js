document.addEventListener('DOMContentLoaded', function() {
  const suits = ["♠", "♡", "♢", "♣"];
  const ranks = ["Ace", 2, 3, 4, 5, 6, 7, 8, 9, 10, "Knave", "Queen", "King"];

  function getDeck() {
    let deck = [];
    for (const suit of suits) {
      for (const rank of ranks) {
        deck.push({ suit: suit, rank: rank });
      }
    }
    return deck; // Removed stray 's' here
  }

  function getRandomCard() {
    const randomIndex = Math.floor(Math.random() * deck.length);
    return deck[randomIndex];
  }

  let deck = getDeck();

  document.getElementById('hit-button').addEventListener('click', function() {
    let randomCard = getRandomCard();
    console.log(`Your card is ${randomCard.rank} of ${randomCard.suit}. Did you bust?`);
  });

  // Expose functions to window for testing
  window.getDeck = getDeck;
  window.getRandomCard = getRandomCard;
});